package com.ts.temp;

/**
 * Clase que simula el bean loginManagedBean
 *
 * @author TechniSupport SAS
 * @version 1.0
 */
public class LoginManagedBean {

	/**
	 * Simulacion campo market
	 */
	private String market = "JA";

	/**
	 * M�todo que obtiene el campo market
	 *
	 * @return the market
	 */
	public String getMarket() {
		return market;
	}

	/**
	 * M�todo que establece al vampo market
	 *
	 * @param market
	 *            El valor del campo market a establecer
	 */
	public void setMarket(String market) {
		this.market = market;
	}
}
