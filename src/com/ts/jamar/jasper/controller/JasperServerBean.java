package com.ts.jamar.jasper.controller;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.util.Base64;

import com.ts.jamar.jasper.models.Recurso;
import com.ts.jamar.jasper.models.ReportUnit;
import com.ts.jamar.jasper.utils.JasperServerHelper;
import com.ts.temp.LoginManagedBean;

/**
 * Bean Administrado que gestiona las interacciones con
 * el servidor de Reportes
 *
 * @author TechniSupport SAS <info@technisupport.com>
 * @version 1.0
 */
public class JasperServerBean {

	/**
	 * Interacci�n con JasperServer
	 */
	private JasperServerHelper helper;
	/**
	 * Recurso de Reporte seleccionado en el bean
	 */
	private Recurso selectedReporte;
	/**
	 * Unidad de Reporte seleccionada
	 */
	private ReportUnit reporte;
	/**
	 * Listado de recursos de reporte
	 */
	private List<Recurso> reportes;

	/**
	 * Mapa de parametros a solicitar
	 */
	private Map<String, Object> solicitud = new HashMap<String, Object>();

	/**
	 * Propiedad administrada para acceder al bean administrado loginManagedBean
	 */
	@ManagedProperty(value = "#{loginManagedBean}")
	private LoginManagedBean loginManagedBean;

	/**
	 * Contenido a descargar;
	 */

	public StreamedContent print;

	/**
	 * URL de PDF generada
	 */

	private String urlPDF = "http://www.publishers.org.uk/_resources/assets/attachment/full/0/2091.pdf";

	/**
	 * Contenido en HTML del Reporte
	 */
	private String htmlReporte = "";

	/**
	 * Obtiene el HTML del reporte con los parametros seleccionados
	 *
	 */
	public void getHTMLPrint() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String msg = "Parametros:\n<br /><ul>";
		for (String key : solicitud.keySet()) {
			if (solicitud.get(key) != null && solicitud.get(key).getClass().equals(Date.class)) {
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				msg = msg + "<li><b>" + key + "</b>: " + formato.format(solicitud.get(key)) + "\n</li>";
			} else if (solicitud.get(key) == null) {
				msg = msg + "<li><b>" + key + "</b>:</li>";
			} else {
				msg = msg + "<li><b>" + key + "</b>: " + solicitud.get(key) + "\n</li>";
			}

		}
		msg = msg + "</ul>";
		solicitud.put("CEmp", loginManagedBean.getMarket());

		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Imprimiendo Reporte",
						"Generando reporte tipo PDF para <b>" + this.reporte.getLabel() + "</b><br />\n" + msg));
		System.out.println("Imprimiendo");
		String urlReporte = helper.obtenerURLReporteGenerado(reporte, "html", (HashMap<String, Object>) solicitud,
				false);


		Document document;
		String login = helper.getUsuario() + ":" + helper.getClave();
		String base64login = Base64.encodeToString(login.getBytes(), false);

		try {
			Connection.Response response = Jsoup.connect(urlReporte).header("Authorization", "Basic " + base64login)
					.method(Connection.Method.GET).timeout(0).execute();
			Map<String, String> cookies = response.cookies();
			document = response.parse();
			Elements imgs = document.select("img");
			Iterator<Element> it = imgs.iterator();
			while (it.hasNext()) {
				Element img = it.next();
				String src = img.attr("src");
				Date inicial = new Date();
				HttpClient client = HttpClientBuilder.create().build();
				HttpGet httpget = new HttpGet(helper.getUrl() + src.replace("/jasperserver/rest_v2/", ""));
				BasicCookieStore cookieStore = new BasicCookieStore();
				BasicClientCookie cookie = new BasicClientCookie("JSESSIONID", cookies.get("JSESSIONID"));
				cookie.setPath(helper.getRuta());
				cookie.setDomain(helper.getDominio());
				cookieStore.addCookie(cookie);
				BasicHeader header = new BasicHeader("Authorization", "Basic " + base64login);
				httpget.addHeader(header);
				HttpContext localContext = new BasicHttpContext();
				localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
				HttpResponse respuesta = client.execute(httpget, localContext);
				String base64Img = Base64.encodeToString(EntityUtils.toByteArray(respuesta.getEntity()), false);
				String dataUrl="data:"+(respuesta.getHeaders("Content-type"))[0].getValue()+";base64,"+base64Img;
				img.attr("src", dataUrl);
				Date fin = new Date();
				System.out.println(fin.getTime() - inicial.getTime());
				// System.out.println(Base64.encodeToString(imgDoc.bodyAsBytes(),
				// false));
			}
		} catch (Exception e) {
			document = Jsoup.parse("<h1>Vacio</h1>");
		}
		this.htmlReporte = document.body().html();
	}

	/**
	 * M�todo que obtiene el HTML del Reporte generado
	 *
	 * @return El contenido en HTML del reporte
	 */
	public String getHtmlReporte() {
		return htmlReporte;
	}

	/**
	 * M�todo que obtiene el bean administrado loginManagedBean;
	 *
	 * @return El bean loginManagedBean
	 */
	public LoginManagedBean getLoginManagedBean() {
		return loginManagedBean;
	}

	/**
	 * Obtiene el reporte con los parametros seleccionados
	 *
	 * @return StreamedContent
	 */
	public StreamedContent getPrint() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String msg = "Parametros:\n<br /><ul>";
		for (String key : solicitud.keySet()) {
			if (solicitud.get(key) != null && solicitud.get(key).getClass().equals(Date.class)) {
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				msg = msg + "<li><b>" + key + "</b>: " + formato.format(solicitud.get(key)) + "\n</li>";
			} else if (solicitud.get(key) == null) {
				msg = msg + "<li><b>" + key + "</b>:</li>";
			} else {
				msg = msg + "<li><b>" + key + "</b>: " + solicitud.get(key) + "\n</li>";
			}

		}
		msg = msg + "</ul>";
		solicitud.put("CEmp", loginManagedBean.getMarket());
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Imprimiendo Reporte",
						"Generando reporte tipo XLS para <b>" + this.reporte.getLabel() + "</b><br />\n" + msg));
		System.out.println("Imprimiendo");
		InputStream is = helper.obtenerReporteGenerado(reporte, "xls", (HashMap<String, Object>) solicitud).getBody();
		print = new DefaultStreamedContent(is, "application/xls", reporte.getLabel() + ".xls");
		return print;
	}

	/**
	 * M�todo que obtiene la unidad de reporte seleccionada
	 *
	 * @return la unidad de reporte seleccionada
	 */
	public ReportUnit getReporte() {
		return reporte;
	}

	/**
	 * M�todo que obtiene la lista de recursos de reporte de JasperServer
	 *
	 * @return Lista de recursos tipo reporte
	 */
	public List<Recurso> getReportes() {

		this.reportes = helper.getReportes();
		return this.reportes;
	}

	/**
	 * Obtiene el recurso de reporte trabajado
	 *
	 * @return El recurso de reporte
	 */
	public Recurso getSelectedReporte() {
		return selectedReporte;
	}

	/**
	 * Obtiene el mapa de parametros de la solicitud
	 *
	 * @return el mapa de parametros de la solicitud
	 */
	public Map<String, Object> getSolicitud() {
		return solicitud;
	}

	/**
	 * Obtiene la URL del PDF a generar
	 *
	 * @return la URL del PDF a generar
	 */
	public String getUrlPDF() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String msg = "Parametros:\n<br /><ul>";
		for (String key : solicitud.keySet()) {
			if (solicitud.get(key) != null && solicitud.get(key).getClass().equals(Date.class)) {
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				msg = msg + "<li><b>" + key + "</b>: " + formato.format(solicitud.get(key)) + "\n</li>";
			} else if (solicitud.get(key) == null) {
				msg = msg + "<li><b>" + key + "</b>:</li>";
			} else {
				msg = msg + "<li><b>" + key + "</b>: " + solicitud.get(key) + "\n</li>";
			}

		}
		msg = msg + "</ul>";
		solicitud.put("CEmp", loginManagedBean.getMarket());
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Imprimiendo Reporte",
						"Generando reporte tipo PDF para <b>" + this.reporte.getLabel() + "</b><br />\n" + msg));
		urlPDF = helper.obtenerURLReporteGenerado(reporte, "pdf", (HashMap<String, Object>) solicitud, true);

		return urlPDF;
	}

	/**
	 * Inicializa las variables requeridas para la interacci�n
	 */
	@PostConstruct
	public void initBean(){
		this.helper = new JasperServerHelper();
	}

	/**
	 * Inicializa los formularios de reporte
	 *
	 * @return la accion
	 */
	public String obtenerReporte(){
		this.htmlReporte = "";
		this.solicitud = new HashMap<String, Object>();
		return "obtener_reporte";
	}

	/**
	 * M�todo que establece el HTML del Reporte generado
	 * @param htmlReporte el html a establecer
	 */
	public void setHtmlReporte(String htmlReporte) {
		this.htmlReporte = htmlReporte;
	}


	/**
	 * M�todo que establece al bean administrado
	 *
	 * @param loginManagedBean
	 *            el bean a establecer
	 */
	public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
		this.loginManagedBean = loginManagedBean;
	}

	/**
	 * Establece la unidad de reporte a trabajar
	 *
	 * @param reporte
	 *            unidad de reporte a trabajar
	 */
	public void setReporte(ReportUnit reporte) {
		this.reporte = reporte;
	}

	/**
	 * Establece los reportes desde una lista de recursos
	 *
	 * @param reportes
	 *            Los reportes a establecer
	 */
	public void setReportes(List<Recurso> reportes) {
		this.reportes = reportes;
	}

	/**
	 * Establece el reporte a trabajar
	 *
	 * @param reporte
	 *            El recurso de reporte seleccionado
	 */
	public void setSelectedReporte(Recurso reporte){
		this.selectedReporte = reporte;
		this.reporte = helper.getReportUnit(this.selectedReporte);
	}

	/**
	 * Establece el mapa de par�metros a solicitar en el reporte
	 *
	 * @param solicitud
	 *            El mapa de par�metros a establecer
	 */
	public void setSolicitud(Map<String, Object> solicitud) {
		this.solicitud = solicitud;
	}

	/**
	 * Establece la URL del PDF a generar
	 *
	 * @param urlPDF
	 *            la URL del PDF a generar
	 */
	public void setUrlPDF(String urlPDF) {
		this.urlPDF = urlPDF;
	}

}
