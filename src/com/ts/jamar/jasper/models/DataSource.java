/**
 *
 */
package com.ts.jamar.jasper.models;

import java.io.Serializable;

import com.ts.jamar.jasper.models.references.DataSourceReference;

/**
 * Clase que modela un DataSource de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class DataSource implements Serializable {
	/**
	 * La UID de serialización
	 */
	private static final long serialVersionUID = -8851034621997393969L;
	/**
	 * La referencia del DataSource
	 */
	private DataSourceReference dataSourceReference;

	/**
	 * Método que obtiene la referencia del DataSource
	 *
	 * @return La referencia del DataSource
	 */
	public DataSourceReference getDataSourceReference() {
		return dataSourceReference;
	}

	/**
	 * Método que establece la referencia del DataSource
	 *
	 * @param dataSourceReference
	 *            La referencia del DataSource a establecer
	 */
	public void setDataSourceReference(DataSourceReference dataSourceReference) {
		this.dataSourceReference = dataSourceReference;
	}
}
