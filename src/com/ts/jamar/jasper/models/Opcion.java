package com.ts.jamar.jasper.models;

/**
 * Clase que modela una opcion de un campo de selecci�n
 *
 * @author TechniSupport SAS
 *
 */
public class Opcion {
	/**
	 * Etiqueta de la opci�n
	 */
	private String etiqueta;
	/**
	 * Valor de la opci�n
	 */
	private String valor;

	/**
	 * M�todo que obtiene la etiqueta de la opci�n
	 *
	 * @return la etiqueta de la opci�n
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * M�todo que obtiene el valor de la opci�n
	 *
	 * @return el valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * M�todo que establece la etiqueta de la opci�n
	 *
	 * @param etiqueta
	 *            la etiqueta a establecer
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * M�todo que establece el valor de la opci�n
	 *
	 * @param valor
	 *            el valor a esablecer
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
}
