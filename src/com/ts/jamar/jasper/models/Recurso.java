package com.ts.jamar.jasper.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Modelo que representa los recursos obtenidos desde JasperServer
 *
 * @author TechniSupport SAS
 * @version 1.0
 */
public class Recurso implements Serializable {
	/**
	 * UID del serializador
	 */
	private static final long serialVersionUID = -6587820443775442868L;
	//
	private Date creationDate;
	private String description;
	private int permissionMask;
	private String label;
	private String uri;
	private String resourceType;
	private int version;

	/**
	 * @return La fecha de creaci�n del Recurso
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @return La descripci�n del recurso
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return La etiqueta del recurso
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return La mascara de permisos del recurso
	 */
	public int getPermissionMask() {
		return permissionMask;
	}

	/**
	 * @return El tipo de recurso
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @return La uri del recurso
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @return La versi�n del recurso
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param creationDate
	 *            La fecha de creaci�n a establecer
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @param description
	 *            La descripci�n a establecer
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param label
	 *            La etiqueta a establecer
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @param permissionMask
	 *            La mascara de permisos a establecer
	 */
	public void setPermissionMask(int permissionMask) {
		this.permissionMask = permissionMask;
	}

	/**
	 * @param resourceType
	 *            El tipo de recurso a establecer
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @param uri
	 *            La uri a establecer
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @param version
	 *            La versi�n a establecer
	 */
	public void setVersion(int version) {
		this.version = version;
	}
}
