package com.ts.jamar.jasper.models.references;

/**
 * Clase abstracta para los modelos referencia de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public abstract class AbstractReference {
	/**
	 * URI de la referencia
	 */
	private String uri;

	/**
	 * Obtiene la URI de la referencia
	 *
	 * @return La URI de la referencia
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Establece la URI de la referencia
	 *
	 * @param uri
	 *            La URI a establecer
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}
}
