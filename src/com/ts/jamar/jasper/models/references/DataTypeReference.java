/**
 *
 */
package com.ts.jamar.jasper.models.references;

/**
 * Clase que modela una referencia a un DataType de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class DataTypeReference extends AbstractReference {

}
