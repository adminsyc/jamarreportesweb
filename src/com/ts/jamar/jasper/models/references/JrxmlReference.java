/**
 *
 */
package com.ts.jamar.jasper.models.references;

/**
 * Clase que modela una referencia a un objeto JRXML de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class JrxmlReference extends AbstractReference {

}
