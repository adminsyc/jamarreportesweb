/**
 *
 */
package com.ts.jamar.jasper.models.references;

/**
 * Clase que modela una referencia a un DataSource de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class DataSourceReference extends AbstractReference {

}
