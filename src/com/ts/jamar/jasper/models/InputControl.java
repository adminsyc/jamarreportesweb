/**
 *
 */
package com.ts.jamar.jasper.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.ts.jamar.jasper.models.references.InputControlReference;

/**
 * Clase que modela un Control de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class InputControl implements Serializable {
	/**
	 * La UID de serializaci�n
	 */
	private static final long serialVersionUID = -8851034621997393969L;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO BOOLEANO
	 */
	public static final int TIPO_CAMPO_BOOLEANO = 1;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO TEXTO SIMPLE
	 */
	public static final int TIPO_CAMPO_SIMPLE = 2;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO LISTA SENCILLA
	 */
	public static final int TIPO_CAMPO_LISTA_SENCILLA = 3;
	/**
	 * CONSTANTE PARA CAMPO DE TIPO LISTA DE BOTONES TIPO RADIO
	 */
	public static final int TIPO_CAMPO_LISTA_RADIO = 8;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO LISTA DE SELECCION MULTIPLE
	 */
	public static final int TIPO_CAMPO_LISTA_MULTIPLE = 6;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO LISTA DE SELECCION MULTIPLE CON CHECKBOX
	 */
	public static final int TIPO_CAMPO_LISTA_MULTIPLE_CHECKBOX = 10;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO DE LISTA DE SELECCION SENCILLA UTILIZANDO
	 * CONSULTAS A FUENTES DE DATOS
	 */
	public static final int TIPO_CAMPO_LISTA_SENCILLA_QUERY = 4;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO DE LISTA DE SELECCION CON BOTONES TIPO
	 * RADIO UTILIZANDO CONSULTAS A FUENTES DE DATOS
	 */
	public static final int TIPO_CAMPO_LISTA_RADIO_QUERY = 9;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO DE LISTA DE SELECCION MULTIPLE UTILIZANDO
	 * CONSULTAS A FUENTES DE DATOS
	 */
	public static final int TIPO_CAMPO_LISTA_MULTIPLE_QUERY = 7;
	/**
	 * CONSTANTE PARA CAMPOS DE TIPO DE LISTA DE SELECCION MULTIPLES CON
	 * CHECKBOX UTILIZANDO CONSULTAS A FUENTES DE DATOS
	 */
	public static final int TIPO_CAMPO_LISTA_MULTIPLE_CHECKBOX_QUERY = 11;
	/**
	 * La referencia del control
	 */
	private InputControlReference inputControlReference;
	/**
	 * La fecha de creaci�n del control
	 */
	private Date creationDate;

	/**
	 * El control es de solo lectura
	 */
	private boolean readOnly;

	/**
	 * El control es visible
	 */
	private boolean visible;

	/**
	 * El control es obligatorio
	 */
	private boolean mandatory;

	/**
	 * M�scara de permisos del control
	 */
	private int permissionMask;

	/**
	 * Tipo de control
	 */
	private int type;

	/**
	 * URI del Control
	 */
	private String uri;

	/**
	 * Tipo de dato del control
	 */
	private DataType dataType;

	/**
	 * Id del Control
	 */
	private String id;
	/**
	 * Etiqueta del control
	 */
	private String label;
	/**
	 * Opciones del control
	 */
	private ArrayList<Opcion> options;
	// {"permissionMask":1,"label":"Test","type":1,"uri":"/Jamar/Reportes/Xtrategizer/Reporte_De_Prueba_files/Test","updateDate":"2016-01-11T11:09:20","version":0}

	/**
	 * M�todo que obtiene la fecha de creaci�n del control
	 *
	 * @return la fecha de creaci�n
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * M�todo que obtiene el tipo de dato del control
	 *
	 * @return El tipo de dato
	 */
	public DataType getDataType() {
		return dataType;
	}

	/**
	 * M�todo que obtiene el id del control
	 *
	 * @return el id del control
	 */
	public String getId() {
		return id;
	}

	/**
	 * M�todo que obtiene la referencia del control
	 *
	 * @return La referencia del control
	 */
	public InputControlReference getInputControlReference() {
		return inputControlReference;
	}

	/**
	 * M�todo que obtiene la etiqueta del control
	 *
	 * @return la etiqueta del control
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * M�todo que obtiene las opciones del control
	 *
	 * @return las opciones del control
	 */
	public ArrayList<Opcion> getOptions() {
		return options;
	}

	/**
	 * M�todo que obtiene la m�scara de permisos del control
	 *
	 * @return la m�scara de permisos del control
	 */
	public int getPermissionMask() {
		return permissionMask;
	}

	/**
	 * M�todo que obtiene el tipo de control
	 *
	 * @return el tipo de control
	 */
	public int getType() {
		return type;
	}

	/**
	 * M�todo que obtiene la URI del control
	 *
	 * @return la URI del control
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * M�todo que permite saber si el control es obligatorio
	 *
	 * @return si es obligatorio
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * M�todo que permite saber si el control es de solo lectura
	 *
	 * @return si es de solo lectura
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * M�todo que permite saber si el control es visible
	 *
	 * @return si es visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * M�todo que establece la fecha de creaci�n del control
	 *
	 * @param creationDate
	 *            La fecha de creaci�n a establecer
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * M�todo que establece el tipo de dato del control
	 *
	 * @param dataType
	 *            El tipo de dato a establecer
	 */
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	/**
	 * M�todo que establece el Id del control
	 *
	 * @param id
	 *            El Id a establecer
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * M�todo que establece la referencia del control
	 *
	 * @param inputControlReference
	 *            La referencia del control a establecer
	 */
	public void setInputControlReference(InputControlReference inputControlReference) {
		this.inputControlReference = inputControlReference;
	}

	/**
	 * M�todo que establece la etiqueta del control
	 *
	 * @param label
	 *            la etiqueta del control
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * M�todo que establece al control como obligatorio o no
	 *
	 * @param mandatory
	 *            Si es obligatorio
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * M�todo que establece las opciones del control
	 *
	 * @param options
	 *            las opciones a establecer
	 */
	public void setOptions(ArrayList<Opcion> options) {
		this.options = options;
	}

	/**
	 * M�todo que establece la m�scara de permisos del control
	 *
	 * @param permissionMask
	 *            la m�scara de permisos del control a establecer
	 */
	public void setPermissionMask(int permissionMask) {
		this.permissionMask = permissionMask;
	}

	/**
	 * M�todo que establece al control como de solo lectura o no
	 *
	 * @param readOnly
	 *            si es de solo lectura
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * M�todo que establece el tipo de control
	 *
	 * @param type
	 *            el tipo de control a establecer
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * M�todo que establece la URI del control
	 *
	 * @param uri
	 *            La URI a establecer
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * M�todo que establece al control como visible o no
	 *
	 * @param visible
	 *            si es visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
