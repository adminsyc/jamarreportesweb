package com.ts.jamar.jasper.models;

import java.io.Serializable;

import com.ts.jamar.jasper.models.references.JrxmlReference;

/**
 * Clase que modela a un objeto JRXML de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class Jrxml implements Serializable{
	/**
	 * El UID de Serialización
	 */
	private static final long serialVersionUID = 7565964356077143658L;
	private JrxmlReference jrxmlFileReference;

	/**
	 * Método que obtiene la referencia del objeto JRXML de JasperServer
	 *
	 * @return La referencia al objeto JRXML
	 */
	public JrxmlReference getJrxmlFileReference() {
		return jrxmlFileReference;
	}

	/**
	 * Método que establece la referencia del objeto JRXML de JasperServer
	 *
	 * @param jrxmlFileReference
	 *            La referencia del objeto JRXML a establecer
	 */
	public void setJrxmlFileReference(JrxmlReference jrxmlFileReference) {
		this.jrxmlFileReference = jrxmlFileReference;
	}
}
