/**
 *
 */
package com.ts.jamar.jasper.models;

import java.io.Serializable;
import java.util.Date;

import com.ts.jamar.jasper.models.references.DataTypeReference;

/**
 * Clase que modela un DataType de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class DataType implements Serializable {
	/**
	 * La UID de serializaci�n
	 */
	private static final long serialVersionUID = -8851034621997393969L;
	/**
	 * La referencia del DataType
	 */
	private DataTypeReference dataTypeReference;
	// {"strictMin":false,"creationDate":"2016-01-10T10:11:33","pattern":"dd/mm/yyyy
	// hh24:mi:ss","strictMax":false,"permissionMask":1,"label":"Fecha y
	// Hora","type":"datetime","uri":"/Jamar/TIPOS_DE_DATOS/FechaHora","updateDate":"2016-01-10T10:11:33","version":0}
	/**
	 * Es estrictamente mayor que {@link #minValue}
	 */
	private boolean strictMin;
	/**
	 * Valor M�nimo
	 */
	private float minValue;
	/**
	 * Es estrictamente mayor que {@link #maxValue}
	 */
	private boolean strictMax;
	/**
	 * Valor M�ximo
	 */
	private float maxValue;
	/**
	 * M�xima Longitud del tipo de dato
	 */
	private int maxLength;
	/**
	 * Fecha de Creaci�n
	 */
	private Date creationDate;
	/**
	 * Fecha de Actualizaci�n
	 */
	private Date updateDate;
	/**
	 * Patr�n
	 */
	private String pattern;
	/**
	 * M�scara de Permisos
	 */
	private int permissionMask;
	/**
	 * Etiqueta
	 */
	private String label;
	/**
	 * Tipo de datos
	 */
	private String type;
	/**
	 * URI
	 */
	private String uri;
	/**
	 * Versi�n
	 */
	private int version;

	/**
	 * M�todo que obtiene la fecha de creaci�n
	 *
	 * @return La fecha de creaci�n
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * M�todo que obtiene la referencia del DataType
	 *
	 * @return La referencia del DataType
	 */
	public DataTypeReference getDataTypeReference() {
		return dataTypeReference;
	}

	/**
	 * M�todo que obtiene la etiqueta del tipo de datos
	 *
	 * @return La etiqueta del tipo de datos
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * M�todo que obtiene la m�xima longitud del tipo de datos
	 *
	 * @return la m�xima longitud del tipo de datos
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * M�todo que obtiene el m�ximo valor permitido del tipo de datos
	 *
	 * @return El valor m�ximo
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/**
	 * M�todo que obtiene el m�nimo valor permitido del tipo de datos
	 *
	 * @return el valor m�nimo
	 */
	public float getMinValue() {
		return minValue;
	}

	/**
	 * M�todo que obtiene el patr�n del tipo de datos
	 *
	 * @return el patr�n del tipo de datos
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * M�todo que obtiene la m�scara de permisos del tipo de datos
	 *
	 * @return La m�scara de permisos
	 */
	public int getPermissionMask() {
		return permissionMask;
	}

	/**
	 * M�todo que obtiene el tipo de datos
	 *
	 * @return el tipo de datos
	 */
	public String getType() {
		return type;
	}

	/**
	 * M�todo que obtiene la fecha de actualizaci�n del tipo de datos
	 *
	 * @return La fecha de actualizaci�n del tipo de datos
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * M�todo que obtiene la URI del tipo de datos
	 *
	 * @return la URI del tipo de datos
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * M�todo que obtiene la versi�n del tipo de datos
	 *
	 * @return la versi�n del tipo de datos
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * M�todo que obtiene si el tipo de datos debe ser estrictamente mayor que
	 * {@link #minValue}
	 *
	 * @return si debe ser estrictamente mayor
	 */
	public boolean isStrictMax() {
		return strictMax;
	}

	/**
	 * M�todo que obtiene si el tipo de datos debe ser estrictamente menor que
	 * {@link #maxValue}
	 *
	 * @return si debe ser estrictamente menor
	 */
	public boolean isStrictMin() {
		return strictMin;
	}

	/**
	 * M�todo que establece la fecha de creaci�n del tipo de datos
	 *
	 * @param creationDate
	 *            la fecha de creaci�n del tipo de datos
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	/**
	 * M�todo que establece la referencia del DataType
	 *
	 * @param dataTypeReference
	 *            La referencia del DataType a establecer
	 */
	public void setDataTypeReference(DataTypeReference dataTypeReference) {
		this.dataTypeReference = dataTypeReference;
	}

	/**
	 * M�todo que establece la etiqueta del tipo de datos
	 *
	 * @param label
	 *            la etiqueta a establecer
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * M�todo que establece la m�xima longitud del tipo de datos
	 *
	 * @param maxLength
	 *            la m�xima longitud a establecer
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * M�todo que establece el valor m�ximo del tipo de datos
	 *
	 * @param maxValue
	 *            el m�ximo valor a establecer
	 */
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * M�todo que establece el valor m�ximo del tipo de datos
	 *
	 * @param minValue
	 *            el valor m�nimo a establecer
	 */
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * M�todo que establece el patr�n del tipo de datos
	 *
	 * @param pattern
	 *            el patr�n a establecer
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * M�todo que establece la m�scara de permisos del tipo de datos
	 *
	 * @param permissionMask
	 *            la m�scara de permisos a establecer
	 */
	public void setPermissionMask(int permissionMask) {
		this.permissionMask = permissionMask;
	}

	/**
	 * M�todo que establece si el tipo de datos debe ser estrictamente menor que
	 * {@link #maxValue}
	 *
	 * @param strictMax
	 *            es estrictamente menor que {@link #maxValue}
	 */
	public void setStrictMax(boolean strictMax) {
		this.strictMax = strictMax;
	}

	/**
	 * M�todo que establece si el tipo de datos debe ser estrictamente mayor que
	 * {@link #minValue}
	 *
	 * @param strictMin
	 *            es estrictamente mayor que {@link #minValue}
	 */
	public void setStrictMin(boolean strictMin) {
		this.strictMin = strictMin;
	}

	/**
	 * M�todo que establece el tipo de dato
	 *
	 * @param type
	 *            el tipo de dato a establecer
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * M�todo que establece la fecha de actualizaci�n del tipo de datos
	 *
	 * @param updateDate
	 *            la fecha de actualizaci�n a establecer
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * M�todo que estable la URI del tipo de datos
	 *
	 * @param uri
	 *            La URI a establecer
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * M�todo que establece la versi�n del tipo de datos
	 *
	 * @param version
	 *            la versi�n a establecer
	 */
	public void setVersion(int version) {
		this.version = version;
	}
}
