package com.ts.jamar.jasper.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que modela a una Unidad de Reporte de JasperServer
 *
 * @author TechniSupport SAS
 *
 */
public class ReportUnit implements Serializable {
	/**
	 * El UID De serializaci�n
	 */
	private static final long serialVersionUID = 2911913658728132269L;

	/**
	 * M�todo que obtiene la UID de serializaci�n
	 *
	 * @return La UID de Serializaci�n
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	private int version;
	private int permissionMask;
	private Date creationDate;
	private Date updateDate;
	private String label;
	private String description;
	private String uri;
	private DataSource dataSource;
	private Jrxml jrxml;
	private InputControl[] inputControls;
	private String inputControlRenderingView;
	private String reportRenderingView;
	private boolean alwaysPromptControls;
	private String controlsLayout;

	/**
	 * M�todo que obtiene la disposici�n de los controles de la unidad de
	 * reporte
	 *
	 * @return La disposici�n de los controles
	 */
	public String getControlsLayout() {
		return controlsLayout;
	}

	/**
	 * M�todo que obtiene la fecha de creaci�n de la unidad de reporte
	 *
	 * @return La fecha de Creaci�n de la Unidad de Reporte
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * M�todo que obtiene el datasource de la unidad de reporte
	 *
	 * @return El datasource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * M�todo que obtiene la descripci�n de la unidad de reporte
	 *
	 * @return la descripci�n
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * M�todo que obtiene la vista de los controles de la unidad de reporte
	 *
	 * @return la vista de los controles
	 */
	public String getInputControlRenderingView() {
		return inputControlRenderingView;
	}

	/**
	 * M�todo que obtiene los controles de la unidad de reporte
	 *
	 * @return Los controles
	 */
	public InputControl[] getInputControls() {
		return inputControls;
	}

	/**
	 * M�todo que obtiene el objeto jrxml del reporte
	 *
	 * @return el objeto jrxml
	 */
	public Jrxml getJrxml() {
		return jrxml;
	}

	/**
	 * M�todo que obtiene la etiqueta de la unidad de reporte
	 *
	 * @return ta etiqueta
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * M�todo que obtiene la m�scara de permisos de la unidad de reporte
	 *
	 * @return la m�scara de permisos
	 */
	public int getPermissionMask() {
		return permissionMask;
	}

	/**
	 * M�todo que obtiene la vista de visualizaci�n del reporte
	 *
	 * @return la vista de visualizaci�n
	 */
	public String getReportRenderingView() {
		return reportRenderingView;
	}

	/**
	 * M�todo que obtiene la fecha de actualizaci�n de la unidad de reporte
	 *
	 * @return la fecha de actualizaci�n
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * M�todo que obtiene la URI de la unidad de reporte
	 *
	 * @return la URI
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * M�todo que obtiene la versi�n de la unidad de reporte
	 *
	 * @return La versi�n
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * Permite saber si se deben mostrar controles siempre en la unidad de
	 * reporte
	 *
	 * @return si se debe mostrar controles siempre
	 */
	public boolean isAlwaysPromptControls() {
		return alwaysPromptControls;
	}

	/**
	 * M�todo para establecer si se deben mostrar siempre los controles de la
	 * unidad de reporte
	 *
	 * @param alwaysPromptControls
	 *            the alwaysPromptControls to set
	 */
	public void setAlwaysPromptControls(boolean alwaysPromptControls) {
		this.alwaysPromptControls = alwaysPromptControls;
	}

	/**
	 * M�todo para establecer la disposici�n de los controles de la unidad de
	 * reporte
	 *
	 * @param controlsLayout
	 *            la disposici�n a establecer
	 */
	public void setControlsLayout(String controlsLayout) {
		this.controlsLayout = controlsLayout;
	}

	/**
	 * M�todo para establecer la fecha de creaci�n de la unidad de reporte
	 *
	 * @param creationDate
	 *            la fecha de creaci�n a establecer
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * M�todo para establecer el datasource de la unidad de reporte
	 *
	 * @param dataSource
	 *            el datasource a establecer
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * M�todo que establece la descripci�n de la unidad de reporte
	 *
	 * @param description
	 *            la descripci�n a establecer
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * M�todo que estable la vista de los controles de la unidad de reporte
	 *
	 * @param inputControlRenderingView
	 *            la vista a establecer
	 */
	public void setInputControlRenderingView(String inputControlRenderingView) {
		this.inputControlRenderingView = inputControlRenderingView;
	}

	/**
	 * M�todo que establece los controles de la unidad de reporte
	 *
	 * @param inputControls
	 *            los controles a establecer
	 */
	public void setInputControls(InputControl[] inputControls) {
		this.inputControls = inputControls;
	}

	/**
	 * M�todo que establece el objeto JRXML de la unidad de reporte
	 *
	 * @param jrxml
	 *            El objeto JRXML de la unidad de reporte
	 */
	public void setJrxml(Jrxml jrxml) {
		this.jrxml = jrxml;
	}

	/**
	 * M�todo que establece la etiqueta de la unidad de reporte
	 *
	 * @param label
	 *            la etiqueta a establecer
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * M�todo que establece la m�scara de permisos de la unidad de reporte
	 *
	 * @param permissionMask
	 *            la mascara de permisos a establecer
	 */
	public void setPermissionMask(int permissionMask) {
		this.permissionMask = permissionMask;
	}

	/**
	 * M�todo que establece la vista de visualizaci�n de la unidad de reporte
	 *
	 * @param reportRenderingView
	 *            la vista de visualizaci�n a establecer
	 */
	public void setReportRenderingView(String reportRenderingView) {
		this.reportRenderingView = reportRenderingView;
	}

	/**
	 * M�todo que establece la fecha de actualizaci�n de la unidad de reporte
	 *
	 * @param updateDate
	 *            la fecha de actualizaci�n de la unidad de reporte
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * M�todo que establece la URI de la unidad de reporte
	 *
	 * @param uri
	 *            La URI a establecer
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * M�todo que establece la versi�n de la unidad de reporte
	 *
	 * @param version
	 *            la versi�n a establecer
	 */
	public void setVersion(int version) {
		this.version = version;
	}
}
