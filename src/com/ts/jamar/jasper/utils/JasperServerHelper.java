package com.ts.jamar.jasper.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jamar.exception.PropertyException;
import com.jamar.properties.facade.IPropertiesFacade;
import com.jamar.properties.facade.impl.PropertiesFacade;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.ts.jamar.jasper.models.DataType;
import com.ts.jamar.jasper.models.InputControl;
import com.ts.jamar.jasper.models.Opcion;
import com.ts.jamar.jasper.models.Recurso;
import com.ts.jamar.jasper.models.ReportUnit;
import com.ts.jamar.jasper.models.references.DataTypeReference;
import com.ts.jamar.jasper.models.references.InputControlReference;

/**
 * Clase que contiene los m�todos y attributos de interacci�n con JasperServer
 *
 * @author TechniSupport SAS
 * @version 1.0
 */
public class JasperServerHelper {
	/**
	 * Llave raiz de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER = "jamar.properties.jasperserver";
	/**
	 * Llave del archivo de propiedades de la url de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_URL = PROPIEDAD_JASPERSERVER + ".url";
	/**
	 * Llave del archivo de propiedades del dominio de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_DOMAIN = PROPIEDAD_JASPERSERVER + ".domain";
	/**
	 * Llave del archivo de propiedades del puerto de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_PORT = PROPIEDAD_JASPERSERVER + ".port";
	/**
	 * Llave del archivo de propiedades de la ruta de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_PATH = PROPIEDAD_JASPERSERVER + ".path";
	/**
	 * Llave del archivo de propiedades de la ruta de REST de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_PATH_REST = PROPIEDAD_JASPERSERVER_PATH + ".rest";
	/**
	 * Llave del archivo de propiedades del usuario de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_USUARIO = PROPIEDAD_JASPERSERVER + ".user";
	/**
	 * Llave del archivo de propiedades de la clave del usuario de JasperServer
	 */
	public final static String PROPIEDAD_JASPERSERVER_CLAVE = PROPIEDAD_JASPERSERVER + ".pass";
	/**
	 * Llave del archivo de propiedades de la URI de la organizaci�n
	 */
	public final static String PROPIEDAD_JASPERSERVER_ROOT = PROPIEDAD_JASPERSERVER + ".root.uri";
	/**
	 * Llave del archivo de propiedades de la URI de Campos
	 */
	public final static String PROPIEDAD_JASPERSERVER_CAMPOS = PROPIEDAD_JASPERSERVER + ".controls.uri";
	/**
	 * Llave del archivo de propiedades de la URI de Tipos de Datos
	 */
	public final static String PROPIEDAD_JASPERSERVER_TIPOS_DE_DATOS = PROPIEDAD_JASPERSERVER + ".datatypes.uri";
	/**
	 * Llave del archivo de propiedades de la URI de Reportes
	 */
	public final static String PROPIEDAD_JASPERSERVER_REPORTES = PROPIEDAD_JASPERSERVER + ".reports.uri";

	/**
	 * Usuario de Jasper Server
	 */
	private String usuario;
	/**
	 * Usuario de Jasper Server
	 */
	private String clave;
	/**
	 * URL de Jasper Server
	 */
	private String url;
	/**
	 * URI De la raiz de la instancia
	 */
	private String uriRaiz;
	/**
	 * URI De los Reportes
	 */
	private String uriReporte;
	/**
	 * URI De los campos
	 */
	private String uriCampos;
	/**
	 * URI de los t�pos de datos
	 */
	private String uriTiposDeDatos;
	/**
	 * Dominio de Jasperserver
	 */
	private String dominio;
	/**
	 * Puerto de Jasperserver
	 */
	private String puerto;

	/**
	 * Ruta de JasperServer
	 */
	private String ruta;

	/**
	 * Ruta del los servicios rest de JasperServer
	 */
	private String rest;

	/**
	 * Constructor de la clase que lee los atributos desde archivo de
	 * propiedades
	 */
	public JasperServerHelper() {
		this.obtenerValores();
	}

	/**
	 * Constructor de la clase que establece valores a los atributos
	 *
	 * @param usuario
	 *            Usuario de JasperServer a establecer
	 * @param clave
	 *            Clave de JasperServer a establecer
	 * @param url
	 *            URL de JasperServer a establecer
	 */
	public JasperServerHelper(String usuario, String clave, String url) {
		super();
		this.usuario = usuario;
		this.clave = clave;
		this.url = url;
	}

	/**
	 * M�todo que obtiene los reportes de JasperServer
	 *
	 * @return Los recuros de reportes
	 */
	public List<Recurso> getCampos() {
		return this.getRecursos("inputControl", this.getUriCampos());
	}

	/**
	 * M�todo que obtiene la clave del usuario de JasperServer
	 *
	 * @return La clave del Usuario de JasperServer
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * M�todo que obtiene el dominio de JasperServer
	 *
	 * @return el dominio
	 */
	public String getDominio() {
		return dominio;
	}

	/**
	 * M�todo que obtiene el puerto de JasperServer
	 * @return el puerto
	 */
	public String getPuerto() {
		return puerto;
	}

	/**
	 * M�todo que obtiene los tipos de datos
	 *
	 * @return Los recursos de tipos de datos
	 */
	public List<Recurso> getRecursos() {
		return this.getRecursos("dataType", this.getUriRaiz());
	}

	/**
	 * M�todo que a partir del tipo de recurso y la uri de este obtiene un
	 * listado de recursos desde JasperServer
	 *
	 * @param tipo
	 *            El tipo de recurso a buscar
	 * @param uri
	 *            La uri de los recursos en JasperServer
	 * @return Lista de Recursos de JasperServer
	 */
	public List<Recurso> getRecursos(String tipo, String uri){
		try{
			this.obtenerValores();
			HttpResponse<JsonNode> node = Unirest.get(this.url + "resources")
					.queryString("type",tipo)
					.queryString("folderUri",uri)
					.header("Accept", "application/json")
					.basicAuth(this.usuario, this.clave).asJson();
			List<Recurso> list = new ArrayList<Recurso>();
			JSONArray recursos;
			try {
				recursos = node.getBody().getObject().getJSONArray("resourceLookup");
				for(int i=0;i<recursos.length();i++){
					JSONObject recursoJSON = recursos.getJSONObject(i);
					Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
					Recurso recurso = gson.fromJson(recursoJSON.toString(), Recurso.class);
					list.add(recurso);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			return list;
		}catch(UnirestException ure){
			ure.printStackTrace();
		}

		return null;
	}
	/**
	 * M�todo que obtiene los reportes de JasperServer
	 *
	 * @return Los recuros de reportes
	 */
	public List<Recurso> getReportes() {
		return this.getRecursos("reportUnit", this.getUriReporte());
	}

	/**
	 * M�todo que obtiene la informaci�n espec�fica de un reporte
	 *
	 * @param reporte
	 *            El recurso a buscar
	 * @return La unidad de Reporte
	 */
	public ReportUnit getReportUnit(Recurso reporte) {
		System.out.println("* OBTENIENDO DETALLE DEL REPORTE");
		HttpResponse<JsonNode> node = this.obtenerDetalleRecurso(reporte);
		JSONObject reportObject = node.getBody().getObject();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		ReportUnit report = gson.fromJson(reportObject.toString(), ReportUnit.class);
		HttpResponse<JsonNode> nodeControles = this.obtenerEstadoReportUnit(report);
		JSONArray arregloControles = new JSONArray();
		try {
			arregloControles = nodeControles.getBody().getObject().getJSONArray("inputControl");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		ArrayList<JSONObject> controlesJson = new ArrayList<JSONObject>();
		if (arregloControles.length() > 0) {
			for (int i = 0; i < arregloControles.length(); i++) {
				try {
					JSONObject objetoControl = arregloControles.getJSONObject(i);
					controlesJson.add(objetoControl);
				} catch (JSONException e) {
				}
			}
		}
		Object listaControles = gson.fromJson(nodeControles.getBody().toString(), Object.class);
		System.out.println("* * OBTENIENDO DETALLE DE LOS CONTROLES");
		int j=0;
		InputControl[] controles = report.getInputControls();
		for (InputControl control : report.getInputControls()) {
			if (control.getInputControlReference() != null && control.getInputControlReference().getUri() != null
					&& !control.getInputControlReference().getUri().trim().isEmpty()) {
				/*
				 * Inicializando controles referenciados
				 */
				Recurso recursoControl = new Recurso();
				recursoControl.setUri(control.getInputControlReference().getUri());
				System.out.println("* * * OBTENIENDO DETALLE DEL CONTROL");
				HttpResponse<JsonNode> nodeControl = this.obtenerDetalleRecurso(recursoControl);
				InputControlReference referencia = control.getInputControlReference();
				control = gson.fromJson(nodeControl.getBody().toString(), InputControl.class);
				control.setInputControlReference(referencia);
				JSONObject objetoControl = controlesJson.get(j);

				try {
					control.setId(objetoControl.getString("id"));
				} catch (JSONException e) {
				}
				control.setOptions(new ArrayList<Opcion>());
				try {
					JSONArray opciones = objetoControl.getJSONObject("state").getJSONArray("options");
					ArrayList<Opcion> opcionesArr = new ArrayList<Opcion>();
					for (int k = 0; k < opciones.length(); k++) {
						JSONObject opcionObj = opciones.getJSONObject(k);
						Opcion opcion = new Opcion();
						opcion.setEtiqueta(opcionObj.getString("label"));
						opcion.setValor(opcionObj.getString("value"));
						control.getOptions().add(opcion);
						opcionesArr.add(opcion);
					}
					control.setOptions(opcionesArr);
				} catch (JSONException e) {
				}

				/*
				 * Inicializando Tipos de datos referenciados
				 */
				if (control.getDataType() != null) {
					Recurso recursoDataType = new Recurso();
					recursoDataType.setUri(control.getDataType().getDataTypeReference().getUri());
					System.out.println("* * * * OBTENIENDO DETALLE DEL TIPO DE DATOS");
					HttpResponse<JsonNode> nodeDataType = this.obtenerDetalleRecurso(recursoDataType);
					DataTypeReference referenciaDataType = control.getDataType().getDataTypeReference();
					DataType dataType = gson.fromJson(nodeDataType.getBody().toString(), DataType.class);
					control.setDataType(dataType);
					dataType.setDataTypeReference(referenciaDataType);

				}
				controles[j] = control;
				j++;
			}
		}
		report.setInputControls(controles);
		return report;
	}

	/**
	 * M�todo que obtiene la ruta de los servicios REST de JasperServer
	 * @return la ruta de los servicios REST
	 */
	public String getRest() {
		return rest;
	}

	/**
	 * M�todo que obtiene  la ruta de JasperServer
	 * @return la ruta
	 */
	public String getRuta() {
		return ruta;
	}

	/**
	 * M�todo que obtiene los tipos de datos
	 *
	 * @return Los recursos de tipos de datos
	 */
	public List<Recurso> getTiposDeDatos() {
		return this.getRecursos("dataType", this.getUriTiposDeDatos());
	}

	/**
	 * M�todo que obtiene la URI de los campos de JasperServer
	 *
	 * @return La URI de los campos
	 */
	public String getUriCampos() {
		return uriCampos;
	}

	/**
	 * M�todo que obtiene la URI Raiz de JasperServer
	 *
	 * @return La URI ra�z
	 */
	public String getUriRaiz() {
		return uriRaiz;
	}

	/**
	 * M�todo que obtiene la URI de los reportes de JasperServer
	 *
	 * @return La URI de los reportes
	 */
	public String getUriReporte() {
		return uriReporte;
	}

	/**
	 * M�todo que obtiene la URI de los tipos de datos de JasperServer
	 *
	 * @return La URI de los tipos de datos
	 */
	public String getUriTiposDeDatos() {
		return uriTiposDeDatos;
	}

	/**
	 * M�todo que obtiene la URL de JasperServer
	 *
	 * @return La URL de JasperServer
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * M�todo que obtiene el Usuario de JasperServer
	 *
	 * @return El Usuario de JasperServer
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * M�todo que obtiene la Informaci�n detallada de un recurso
	 *
	 * @param recurso
	 *            El recurso a buscar
	 * @return Respuesta con el detalle del recurso
	 */
	public HttpResponse<JsonNode> obtenerDetalleRecurso(Recurso recurso){
		try {
			HttpResponse<JsonNode> nodo = Unirest.get(this.url + "resources" + recurso.getUri())
					.header("Accept", "application/json").basicAuth(this.usuario, this.clave).asJson();
			System.out.println(nodo.getBody());
			return nodo;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * M�todo que obtiene el estado de los campos de un reporte
	 *
	 * @param recurso
	 *            El reporte a buscar
	 * @return Respuesta con el estado de los campos de un reporte
	 */
	public HttpResponse<JsonNode> obtenerEstadoReportUnit(ReportUnit recurso) {
		try {
			HttpResponse<JsonNode> nodo = Unirest.get(this.url + "reports" + recurso.getUri() + "/inputControls")
					.header("Accept", "application/json").basicAuth(this.usuario, this.clave).asJson();
			System.out.println(nodo.getBody());
			return nodo;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * M�todo que genera un reporte con los parametros solicitadps
	 *
	 * @param reporte
	 *            El reporte a generar
	 * @param formato
	 *            La extensi�n a generar
	 * @param parametros
	 *            Los parametros a filtrar
	 * @return Respuesta con el estado de los campos de un reporte
	 */
	public HttpResponse<InputStream> obtenerReporteGenerado(ReportUnit reporte, String formato,
			HashMap<String, Object> parametros) {
		String url = obtenerURLReporteGenerado(reporte, formato, parametros, false);
		try {

			HttpResponse<InputStream> nodo = Unirest.get(URI.create(url).toASCIIString())
					.basicAuth(this.usuario, this.clave).asBinary();
			// System.out.println(nodo.getBody());
			return nodo;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * M�todo que obtiene una URL a generar
	 *
	 * @param reporte
	 *            El reporte a generar
	 * @param formato
	 *            La extensi�n a generar
	 * @param parametros
	 *            Los parametros a filtrar
	 * @param autenticada
	 *            con autenticacion
	 *
	 * @return La URL generada
	 */
	public String obtenerURLReporteGenerado(ReportUnit reporte, String formato, HashMap<String, Object> parametros,
			boolean autenticada) {
		String url = "";
		String parametrosStr = "";
		if (parametros.size() > 0) {
			parametrosStr = "?";
			for (String key : parametros.keySet()) {
				if (parametros.get(key) != null && parametros.get(key).getClass().equals(Date.class)) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
					parametrosStr = parametrosStr + key + "="
							+ URLEncoder.encode(format.format(parametros.get(key))) + "&";
				} else if (parametros.get(key) == null) {
					parametrosStr = parametrosStr + key + "=&";
				} else {
					parametrosStr = parametrosStr + key + "=" + URLEncoder.encode((String) parametros.get(key))
					+ "&";
				}
			}
		}
		url = this.url + "reports" + reporte.getUri() + "." + formato + parametrosStr;
		if (autenticada) {
			url = url.replace("http://", "http://" + this.usuario + ":" + this.clave + "@");
		}
		return url;
	}

	/**
	 * M�todo que obtiene los valores de las propiedades
	 */
	public void obtenerValores() {
		IPropertiesFacade facade = new PropertiesFacade();
		try {
			this.dominio = facade.getProperty(PROPIEDAD_JASPERSERVER_DOMAIN);
			this.puerto = facade.getProperty(PROPIEDAD_JASPERSERVER_PORT);
			this.ruta = facade.getProperty(PROPIEDAD_JASPERSERVER_PATH);
			this.rest = facade.getProperty(PROPIEDAD_JASPERSERVER_PATH_REST);
			this.url = "http://" + this.dominio + ":" + this.puerto + this.ruta + this.rest;
			this.usuario = facade.getProperty(PROPIEDAD_JASPERSERVER_USUARIO);
			this.clave = facade.getProperty(PROPIEDAD_JASPERSERVER_CLAVE);
			this.uriRaiz = facade.getProperty(PROPIEDAD_JASPERSERVER_ROOT);
			this.uriCampos = this.uriRaiz + facade.getProperty(PROPIEDAD_JASPERSERVER_CAMPOS);
			this.uriReporte = this.uriRaiz + facade.getProperty(PROPIEDAD_JASPERSERVER_REPORTES);
			this.uriTiposDeDatos = this.uriRaiz + facade.getProperty(PROPIEDAD_JASPERSERVER_TIPOS_DE_DATOS);
		} catch (PropertyException e) {
			e.printStackTrace();
		}
	}

	/**
	 * M�todo que establece la clave del usuario de JasperServer
	 *
	 * @param clave
	 *            La clave del usuario a establecer
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * M�todo que establece el dominio de JasperServer
	 * @param dominio el dominio a establecer
	 */
	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	/**
	 * M�todo que establece el puerto de JasperServer
	 * @param puerto el puerto a establecer
	 */
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	/**
	 * M�todo que obtiene la ruta de los servicios REST de JasperServer
	 * @param rest la ruta de los servicios REST a establecer
	 */
	public void setRest(String rest) {
		this.rest = rest;
	}

	/**
	 * M�todo que establece la ruta de JasperServer
	 * @param ruta la ruta a establecer
	 */
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	/**
	 * M�todo que establece la URI de los campos en JasperServer
	 *
	 * @param uriCampos
	 *            La URI a establecer
	 */
	public void setUriCampos(String uriCampos) {
		this.uriCampos = uriCampos;
	}

	/**
	 * M�todo que establece la URI Raiz de JasperServer
	 *
	 * @param uriRaiz
	 *            La URI a establecer
	 */
	public void setUriRaiz(String uriRaiz) {
		this.uriRaiz = uriRaiz;
	}

	/**
	 * M�todo que establece la URI de los Reportes en JasperServer
	 *
	 * @param uriReporte
	 *            La URI a establecer
	 */
	public void setUriReporte(String uriReporte) {
		this.uriReporte = uriReporte;
	}

	/**
	 * M�todo que establece la URI de los tipos de datos en JasperServer
	 *
	 * @param uriTiposDeDatos
	 *            La URI a establecer
	 */
	public void setUriTiposDeDatos(String uriTiposDeDatos) {
		this.uriTiposDeDatos = uriTiposDeDatos;
	}

	/**
	 * M�todo que establece la URL de JasperServer
	 *
	 * @param url
	 *            La URL de JasperServer a establecer
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * M�todo que establece el usuario de JasperServer
	 *
	 * @param usuario
	 *            La clave del usuario a establecer
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * M�todo que convierte un stream a cadena
	 *
	 * @param in
	 *            Stream a convertir
	 * @return Cadena Convertida
	 * @throws IOException
	 */
	public String streamToString(InputStream in) throws IOException {
		StringBuilder out = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			out.append(line);
		}
		br.close();
		return out.toString();
	}
}
